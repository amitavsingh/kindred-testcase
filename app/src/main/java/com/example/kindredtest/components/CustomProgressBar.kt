package com.example.kindredtest.components

import android.app.Dialog
import android.content.Context
import android.graphics.Point
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import com.example.kindredtest.R


class CustomProgressBar(context: Context) : Dialog(context) {

    private lateinit var textView: TextView
    private lateinit var container: FrameLayout
    private lateinit var cardView: CardView
    private var ballSpinView: BallSpinView? = null

    init {
        init()
    }

    private fun init() {
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.layout_progress_bar)
        container = findViewById(R.id.progress_bar_container)
        textView = findViewById(R.id.message)
        cardView = findViewById(R.id.progress_card)
    }



    fun setText(text: String?, style: Int = R.style.LoaderMessageTextview) {
        TextViewCompat.setTextAppearance(textView, style)
        if (text != null) {
            textView.text = text
        }
    }

    fun setBallSpinLoader() {
        ballSpinView = BallSpinView(context)
        val params = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        params.gravity = Gravity.CENTER
        container.addView(ballSpinView, params)
    }



    fun cancelSpinAnimation() {
        if (ballSpinView != null)
            ballSpinView!!.cancelAnimation()
    }



    private fun setFullScreen() {
        cardView.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.background_light))
        val defaultDisplay =
            (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        val size = Point()
        defaultDisplay.getSize(size)
        cardView.layoutParams = FrameLayout.LayoutParams(size.x, size.y)
        cardView.radius = 0f
    }

    data class Builder(var context: Context? = null) {

        private var message: String? = null
        private var dialog: CustomProgressBar? = null
        private var cancelable: Boolean = false

        fun setMessage(message: String) = apply { this.message = message }
        fun setCancelable(cancelable: Boolean) = apply { this.cancelable = cancelable }

        fun startSpinLoader() {
            dialog = CustomProgressBar(context!!)
            dialog?.setText(message)
            dialog?.setFullScreen()
            dialog?.setCancelable(cancelable)
            dialog?.setBallSpinLoader()
            dialog?.show()
        }

        fun dismiss() {
            if (dialog != null) {
                dialog?.cancelSpinAnimation()
                dialog?.dismiss()
            }
        }
    }
}
