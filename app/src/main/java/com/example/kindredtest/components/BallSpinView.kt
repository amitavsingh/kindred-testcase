package com.example.kindredtest.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.Transformation
import com.example.kindredtest.R
import kotlin.math.cos
import kotlin.math.sin


class BallSpinView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : View(context, attrs) {

    private lateinit var colorArray: Array<String>
    private val progressPaint = Paint()
    private val bounceAnimation = RepeatAnimation()
    private var count: Int = 1

    public override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        colorArray = context.resources.getStringArray(R.array.ball_spin)
        bounceAnimation.duration = DURATION_OF_EACH_ANIMATION
        bounceAnimation.repeatCount = Animation.INFINITE
        bounceAnimation.interpolator = LinearInterpolator()
        startAnimation()
    }

    public override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.translate((this.width / 2).toFloat(), (this.height / 2).toFloat())

        try {
            Thread.sleep(DELAY_AFTER_EACH_ANIMATION)
        } catch (e: InterruptedException) {
            // Interrupted
        }

        colorArray = shiftElementToRight(colorArray)
        createDotsAndFormCircle(canvas, progressPaint)
    }

    private fun createDotsAndFormCircle(canvas: Canvas, progressPaint: Paint) {

        for (i in 1..DOT_COUNT) {
            progressPaint.color = Color.parseColor(colorArray[i - 1])
            val x = (CIRCLE_RADIUS * cos(ANGLE * i * (Math.PI / 180)))
            val y = (CIRCLE_RADIUS * sin(ANGLE * i * (Math.PI / 180)))
            canvas.drawCircle(x.toFloat(), y.toFloat(), DOT_RADIUS.toFloat(), progressPaint)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val width: Int = 100 + DOT_RADIUS * 3
        val height: Int = 100 + DOT_RADIUS * 3

        setMeasuredDimension(width, height)
    }

    private fun startAnimation() {

        bounceAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                // Do Nothing
            }
            override fun onAnimationEnd(animation: Animation) {
                // Do Nothing
            }
            override fun onAnimationRepeat(animation: Animation) {
                if (count < DOT_COUNT) {
                    count++
                } else {
                    count = 1
                }
            }
        })
        startAnimation(bounceAnimation)
    }

    private fun shiftElementToRight(array: Array<String>): Array<String> {
        val temp: String = array[array.size - 1]
        for (i in array.size - 2 downTo 0) {
            array[i + 1] = array[i]
        }
        array[0] = temp
        return array
    }

    fun cancelAnimation() {
        bounceAnimation.cancel()
    }

    inner class RepeatAnimation : Animation() {
        override fun applyTransformation(
            interpolatedTime: Float,
            t: Transformation?
        ) {
            super.applyTransformation(interpolatedTime, t)
            this@BallSpinView.invalidate()
        }
    }

    companion object {
        private const val DOT_COUNT = 8
        private const val DOT_RADIUS = 10
        private const val CIRCLE_RADIUS = 50
        private const val ANGLE = 360 / DOT_COUNT
        private const val DELAY_AFTER_EACH_ANIMATION = 100L
        private const val DURATION_OF_EACH_ANIMATION = 100L
    }
}