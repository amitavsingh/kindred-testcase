package com.example.kindredtest.data.repositories

import com.example.kindredtest.data.network.APIRequestBuilder
import com.example.kindredtest.utils.Constants

class GamesRepository(private val apiRequestBuilder: APIRequestBuilder) : ApiRequest() {
    suspend fun getGames() = apiRequest { apiRequestBuilder.getGames(Constants.GAME_LIST_URL) }
}