package com.example.kindredtest.data.network

import com.example.kindredtest.data.models.GamesResponse
import com.example.kindredtest.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url


interface APIRequestBuilder {

    @GET
    suspend fun getGames(@Url url: String) : Response<GamesResponse>

    companion object {
        operator fun invoke() : APIRequestBuilder {

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .client(httpClient.build())
                .build()
                .create(APIRequestBuilder::class.java)
        }
    }
}