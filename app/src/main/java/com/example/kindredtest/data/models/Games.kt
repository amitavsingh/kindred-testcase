package com.example.kindredtest.data.models

data class Games(
    val gameName: String,
    val imageUrl: String,
    val gameId: String
)