package com.example.kindredtest

import android.app.Application
import com.example.kindredtest.data.network.APIRequestBuilder
import com.example.kindredtest.data.repositories.GamesRepository
import com.example.kindredtest.ui.games.GamesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class TestApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@TestApplication))

        bind() from singleton { APIRequestBuilder() }
        bind() from singleton { GamesRepository(instance()) }
        bind() from provider { GamesViewModelFactory(instance()) }
    }
}