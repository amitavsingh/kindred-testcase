package com.example.kindredtest.utils

object Constants {
    const val BASE_URL = "https://api.unibet.com/game-library-rest-api/"
    const val GAME_LIST_URL = "getGamesByMarketAndDevice.json?jurisdiction=UK&brand=unibet&deviceGroup=mobilephone&locale=en_GB&currency=GBP&categories=casino,softgames&clientId=casinoapp"
}