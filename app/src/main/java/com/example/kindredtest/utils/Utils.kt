package com.example.kindredtest.utils

import androidx.lifecycle.MutableLiveData
import com.example.kindredtest.data.models.Games
import com.example.kindredtest.data.models.GamesResponse
import com.google.gson.Gson
import org.json.JSONObject
import org.json.JSONException

object Utils {
    fun getGamesItems(returnString: GamesResponse): List<Games> {
        var gamesList : MutableList<Games> = arrayListOf()
        val data = Gson().toJson(returnString)
        val gamesListData =  JSONObject(data).getJSONObject("games")
        val iterator: Iterator<String> = gamesListData.keys()
        while (iterator.hasNext()) {
            val result = iterator.next()
            val dataObject =  gamesListData.getJSONObject(result)
            val data = Gson().fromJson(dataObject.toString(), Games::class.java)
            gamesList.add(data)
        }
        return gamesList
    }


    @Throws(JSONException::class)
    fun parse(json: JSONObject, out: MutableMap<String?, String?>): Map<String?, String?>? {
        val keys = json.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            var data: String? = null
            if (json.getJSONObject(key) is JSONObject) {
                val value = json.getJSONObject(key)
                parse(value, out)
            } else {
                data = json.getString(key)
            }
            if (data != null) {
                out[key] = data
            }
        }
        return out
    }
}