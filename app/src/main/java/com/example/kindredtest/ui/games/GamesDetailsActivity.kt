package com.example.kindredtest.ui.games

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kindredtest.components.CustomProgressBar
import com.example.kindredtest.data.models.Games
import com.example.kindredtest.data.models.GamesResponse
import com.example.kindredtest.databinding.LayoutGamesDetailActivityBinding
import com.example.kindredtest.utils.Coroutines
import com.example.kindredtest.utils.Utils
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class GamesDetailsActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: GamesViewModelFactory by instance()
    private lateinit var viewModel: GamesViewModel
    private lateinit var binding: LayoutGamesDetailActivityBinding
    private lateinit var loaderBuilder : CustomProgressBar.Builder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutGamesDetailActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
    }

    private fun initViews(){

        supportActionBar?.title = "Games"
        viewModel = ViewModelProvider(this, factory).get(GamesViewModel::class.java)
        loaderBuilder = CustomProgressBar.Builder(this).apply {
            setCancelable(false)
            setMessage("Fetching data...")
        }
        loaderBuilder.startSpinLoader()
        viewModel.getGames()
        viewModel.games.observe(this, Observer { games ->
            getData(games)
        })
    }

    private fun getData(games: GamesResponse) {
        Coroutines.ioThenMain(
            { Utils.getGamesItems(games)},
            { setDataInRecyclerView(it!!) }
        )
    }

    private fun setDataInRecyclerView(gamesList: List<Games>) {
        binding.itemsRecyclerview.also {
            it.layoutManager = LinearLayoutManager(this)
            it.setHasFixedSize(true)
            it.adapter = GamesAdapter(gamesList, context = this)
        }
        stopLoader()
    }

    private fun stopLoader() {
        loaderBuilder.dismiss()
    }
}