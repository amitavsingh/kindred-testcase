package com.example.kindredtest.ui.games

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kindredtest.data.repositories.GamesRepository

@Suppress("UNCHECKED_CAST")
class GamesViewModelFactory(
    private val repository: GamesRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GamesViewModel(repository) as T
    }
}