package com.example.kindredtest.ui.games

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kindredtest.R
import com.example.kindredtest.data.models.Games
import com.example.kindredtest.databinding.LayoutRecyclerviewItemsBinding

class GamesAdapter(private val games: List<Games>, private val context: Context) :
    RecyclerView.Adapter<GamesAdapter.ItemsViewHolder>() {

    override fun getItemCount() = games.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemsViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.layout_recyclerview_items,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.layoutRecyclerviewItemsBinding.item = games[position]
        Glide.with(holder.layoutRecyclerviewItemsBinding.imageView.context)
            .load(games[position].imageUrl)
            .into(holder.layoutRecyclerviewItemsBinding.imageView)


    }


    inner class ItemsViewHolder(
        val layoutRecyclerviewItemsBinding: LayoutRecyclerviewItemsBinding
    ) : RecyclerView.ViewHolder(layoutRecyclerviewItemsBinding.root)


}