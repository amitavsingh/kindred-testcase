package com.example.kindredtest.ui.games

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kindredtest.data.models.GamesResponse
import com.example.kindredtest.data.repositories.GamesRepository
import com.example.kindredtest.utils.Coroutines
import kotlinx.coroutines.Job

class GamesViewModel(private val repository: GamesRepository) : ViewModel()  {

    private lateinit var job: Job

    private val _games = MutableLiveData<GamesResponse>()
    val games: LiveData<GamesResponse>
        get() = _games

    fun getGames() {
        job = Coroutines.ioThenMain(
            { repository.getGames() },
            { _games.value = it }
        )
    }


    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }
}